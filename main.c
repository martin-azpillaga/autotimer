#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <signal.h>
#include <string.h>

Display* connectToDefaultDisplayServer();
Window* getFocusedWindow();
void listenForEvents();
void stopListeningEvents();
XEvent waitForNextEvent();
long int secondsSinceEpoch();
char *getWindowName();
void logStatus();
void signalHandler(int);

Display *display;
Window *window;
FILE *logFile;
int keyPresses;

int main()
{
	printf("---\n");

	connectToDefaultDisplayServer();
	getFocusedWindow();
	listenForEvents();

	keyPresses = 0;

	logStatus();

	signal(SIGINT, signalHandler);
	signal(SIGTERM, signalHandler);

	while (True)
    {
		XEvent event = waitForNextEvent();

        switch (event.type)
        {
            case FocusOut:

				stopListeningEvents();

				getFocusedWindow();
				listenForEvents();

				logStatus();

				keyPresses = 0;

				break;

			case KeyPress:
				keyPresses++;
				break;
		}
	}
}

Display* connectToDefaultDisplayServer()
{
	display = XOpenDisplay(NULL);
}

Window* getFocusedWindow()
{
	window = malloc(sizeof(Window));
	int temp;
	XGetInputFocus(display, window, &temp);
}

void listenForEvents()
{
	XSelectInput(display, *window, KeyPressMask | FocusChangeMask);
}

void stopListeningEvents()
{
	int noEvents = 0;
	XSelectInput(display, *window, noEvents);
}

XEvent waitForNextEvent()
{
	XEvent* event = malloc(sizeof(XEvent));
	XNextEvent(display, event);
	return *event;
}

char* getWindowName()
{
	Atom atom = XInternAtom(display, "_NET_WM_NAME", True);

	Atom actualType;
	int actualFormat;
	unsigned long number, bytes;
	unsigned char *list;

	XGetWindowProperty(display, *window, atom, 0, 100, False, AnyPropertyType, &actualType, &actualFormat, &number, &bytes, &list);

	return list;
}

long int secondsSinceEpoch()
{
	return time(NULL);
}

void logStatus()
{
	char* name = getWindowName(display, window);
	long int seconds = secondsSinceEpoch();

	printf("%ld %d %s\n", seconds, keyPresses, name);
}

void logLast()
{
	long int seconds = secondsSinceEpoch();

	printf("%ld %d %s\n", seconds, keyPresses, "last");
}

void signalHandler(int signum)
{
	logLast();
	exit(0);
}
